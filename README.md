###SETUP
1. Run `mkdir -p ~/projects/`
1. Run `git clone git@gitlab.com:jairusmartin/coverity-connect.git ~/projects/`
1. configure etc/nginx/conf.d/some.site.com.conf
  Replace instances of `some.site.com` with your domain.
1. copy `.env-dist` to `.env` 
   Note: docker uses .env variables for the build and deploy.
1. fetch installer and license, place in build folder and rename (see placeholders)
1. TBD Run gethostname inside of docker container.
1. Navitgate to [Synopsys Community](https://community.synopsys.com/) and run the reshost script inside the docker container.
1. update .env with the license file and install path names that were placed in the build folder

###RUN
1. Run `docker-compose --project-directory=~/projects/coverity-connect up -d`

